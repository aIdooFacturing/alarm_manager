
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String sDate;
	String eDate;
	String startDateTime;
	String endDateTime;
	String delayTimeSec;
	String ncAlarmNum1;
	String ncAlarmNum2;
	String ncAlarmNum3;
	String ncAlarmMsg1;
	String ncAlarmMsg2;
	String ncAlarmMsg3;
	String group;
	
	//common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
}
